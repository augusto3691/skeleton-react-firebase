import React, { Component } from 'react';

import { compose } from 'recompose';

import { withAuthorization } from '../../containers/Session';
import * as ROLES from '../../constants/roles';
import { withFirebase } from '../../containers/Firebase';

class Admin extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loading: false,
            users: [],
        };
    }

    componentDidMount() {
        this.setState({ loading: true });

        this.props.firebase.users().on('value', snapshot => {
            const usersObject = snapshot.val();

            const usersList = Object.keys(usersObject).map(key => ({
                ...usersObject[key],
                uid: key,
            }));

            this.setState({
                users: usersList,
                loading: false,
            });
        });
    }

    componentWillUnmount() {
        this.props.firebase.users().off();
    }

    render() {
        const { users, loading } = this.state;
        return (
            <div>
                <h1>Admin</h1>

                <p>
                    You only see that if you are admin
                        </p>

                {loading && <div>Loading ...</div>}
                <UserList users={users} />
            </div>
        );
    }

}


const UserList = ({ users }) => (
    <ul>
        {users.map(user => (
            <li key={user.uid}>
                <span>
                    <strong>ID:</strong> {user.uid}
                </span>
                <br />
                <span>
                    <strong>E-Mail:</strong> {user.email}
                </span>
                <br />
                <span>
                    <strong>Username:</strong> {user.username}
                </span>
                <br />
                {user.roles &&
                    <span>
                        <strong>Roles:</strong> {user.roles.join()}
                    </span>
                }
            </li>
        ))}
    </ul>
);

const condition = authUser =>
    authUser && authUser.roles.includes(ROLES.ADMIN);

export default compose(
    withAuthorization(condition),
    withFirebase,
)(Admin);