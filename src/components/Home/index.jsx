import React, { Component } from 'react';
import * as ROLES from "../../constants/roles";

import { AuthUserContext, withAuthorization } from "../../containers/Session";

class Home extends Component {
    render() {
        return (
            <div>
                <h1>Home</h1>
                <AuthUserContext.Consumer>
                    {authUser => (
                        <div>
                            {authUser.roles.includes(ROLES.ADMIN) && (
                                "You only will see that if you are admin"
                            )}
                        </div>
                    )}
                </AuthUserContext.Consumer>
            </div>
        );
    }

};

const condition = authUser => !!authUser; // just check if its not null

export default withAuthorization(condition)(Home);