# React Firebase Skeleton Proto

Front End Skeleton for building applications using react and firebase

![](https://s3-sa-east-1.amazonaws.com/static-c4/skeleton.jpg)

## Prerequisite :white_check_mark:

* [node](https://nodejs.org/en/) - v10.15 ou superior
* [yarn](https://yarnpkg.com/pt-BR/) - v1.15 ou superior

## Getting Started :zap:
    git clone git@bitbucket.org:augusto3691/skeleton-react-firebase.git
    cd skeleton-react-firebase
    yarn install

Edit `.env.dist` file with your credentials from firebase and rename it to `.env`, you are good to go :ok_hand:

    yarn start

## Deployment :rocket:

    yarn build
    //The static files will be inside build folder

## Build With :package:

* [create-react-app](https://github.com/facebook/create-react-app) - Components
* [firebase](https://www.npmjs.com/package/firebase) - Serverless
